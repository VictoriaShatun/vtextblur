(function(a,b){"object"==typeof exports&&"undefined"!=typeof module?module.exports=b():"function"==typeof define&&define.amd?define(b):(a=a||self,
	a.BlurPage=b())})
(this,function()
{'use strict';
function a(){}
function b(a,b){for(const c in b)a[c]=b[c];return a}
function c(a){return a()}
function d(){return Object.create(null)}
function e(a){a.forEach(c)}
function f(a){return"function"==typeof a}
function g(c,a){return c==c?c!==a||c&&"object"==typeof c||"function"==typeof c:a==a}
function h(a,b,c){if(a){const d=i(a,b,c);return a[0](d)}}
function i(a,c,d){return a[1]?b({},b(c.$$scope.ctx,a[1](d?d(c):{}))):c.$$scope.ctx}
function j(a,c,d,e){return a[1]?b({},b(c.$$scope.changed||{},a[1](e?e(d):{}))):c.$$scope.changed||{}}
function k(a,b){a.appendChild(b)}
function l(a,b,c){a.insertBefore(b,c||null)}
function m(a){a.parentNode.removeChild(a)}
function n(a){return document.createElement(a)}
function o(a){return document.createTextNode(a)}
function p(){return o(" ")}
function q(){return o("")}
function r(a,b,c,d){return a.addEventListener(b,c,d),()=>a.removeEventListener(b,c,d)}
function s(a){return Array.from(a.childNodes)}
function t(a,b){b=""+b,a.data!==b&&(a.data=b)}
function u(a,b){const c=document.createEvent("CustomEvent");
return c.initCustomEvent(a,!1,!1,b),c}
function v(a){Z=a}
function w(){if(!Z)throw new Error(`Function called outside component initialization`);
return Z}
function x(a){w().$$.on_destroy.push(a)}
function y(){const a=Z;
return(b,c)=>{const d=a.$$.callbacks[b];if(d){const e=u(b,c);d.slice().forEach(b=>{b.call(a,e)})}}}
function z(){aa||(aa=!0,_.then(B))}
function A(a){ca.push(a)}
function B(){const a=new Set;
do{for(;$.length;){const a=$.shift();v(a),
	C(a.$$)}for(;ba.length;)ba.shift()();for(;ca.length;){const b=ca.pop();a.has(b)||(b(),a.add(b))}}
while($.length);for(;da.length;)da.pop()();aa=!1}
function C(a){a.fragment&&(a.update(a.dirty),
	e(a.before_render),
	a.fragment.p(a.dirty,a.ctx),
	a.dirty=null,
	a.after_render.forEach(A))}
function D(){ea={remaining:0,callbacks:[]}}
function E(){ea.remaining||e(ea.callbacks)}
function F(a){ea.callbacks.push(a)}
function G(a,b,d){const{fragment:g,
	on_mount:h,
	on_destroy:i,
	after_render:j}=a.$$;
g.m(b,d),
	A(()=>{const b=h.map(c).filter(f);
	i?i.push(...b):e(b),
		a.$$.on_mount=[]}),
	j.forEach(A)}
function H(a,b){a.$$&&(e(a.$$.on_destroy),
	a.$$.fragment.d(b),a.$$.on_destroy=a.$$.fragment=null,a.$$.ctx={})}
function I(a,b){a.$$.dirty||($.push(a),z(),
	a.$$.dirty={}),a.$$.dirty[b]=!0}
function J(b,c,f,g,h,i){const j=Z;v(b);
const k=c.props||{},l=b.$$={fragment:null,ctx:null,props:i,update:a,
	not_equal:h,bound:d(),on_mount:[],on_destroy:[],before_render:[],
	after_render:[],context:new Map(j?j.$$.context:[]),
	callbacks:d(),dirty:null};let m=!1;
	l.ctx=f?f(b,k,(a,c)=>{l.ctx&&h(l.ctx[a],l.ctx[a]=c)&&(l.bound[a]&&l.bound[a](c),
	m&&I(b,a))}):k,l.update(),m=!0,e(l.before_render),l.fragment=g(l.ctx),
	c.target&&(c.hydrate?l.fragment.l(s(c.target)):l.fragment.c(),c.intro&&b.$$.fragment.i&&b.$$.fragment.i(),
		G(b,c.target,c.anchor),B()),v(j)}
function K(){var b;
return{c(){b=n("div"),
		b.className="___blur-about-container svelte-kidrsn"},m(a,c){l(a,b,c)},p:a,i:a,o:a,d(a){a&&m(b)}}}
			    function L(a){var b,c,d,e,f,g,q,s,u,v;
			    const w=a.$$slots.default,x=h(w,a,null);
			    return{c(){b=n("div"),c=n("div"),d=n("div"),e=o(a.header),f=p(),
							g=n("button"),g.textContent="\u2715",q=p(),s=n("div"),x&&x.c(),
							g.className="svelte-1e1hqwk",d.className="___blur-modal-header svelte-1e1hqwk",
							s.className="___blur-modal-body svelte-1e1hqwk",c.className="___blur-modal svelte-1e1hqwk",
							b.className="___blur-modal-overlay svelte-1e1hqwk",v=r(g,"click",a.click_handler)},
						l(a){x&&x.l(div1_nodes)},m(a,h){l(a,b,h),k(b,c),k(c,d),k(d,e),k(d,f),k(d,g),k(c,q),k(c,s),
						x&&x.m(s,null),u=!0},p(a,b){(!u||a.header)&&t(e,b.header),x&&x.p&&a.$$scope&&x.p(j(w,b,a),
							i(w,b,null))},i(a){u||(x&&x.i&&x.i(a),u=!0)},o(a){x&&x.o&&x.o(a),u=!1},d(a){a&&m(b),
						x&&x.d(a),v()}}}
			    function M(a,b,c){const d=y();
			    let{header:e=""}=b,{$$slots:g={},$$scope:f}=b;
			    return a.$set=a=>{"header"in a&&c("header",e=a.header),"$$scope"in a&&c("$$scope",f=a.$$scope)},
						{dispatch:d,header:e,click_handler:function(){return d("close")},$$slots:g,$$scope:f}}
			    function N(a){var b,c,d,e,f,g;
			    const q=a.$$slots.default,r=h(q,a,null);
			    return{c(){b=n("span"),c=n("span"),d=o(a.tip),e=p(),r&&r.c(),
							c.className="___blur-tooltip-content",b.className=f="___blur-tooltip "+a.clazz},
						l(a){r&&r.l(span1_nodes)},m(a,f){l(a,b,f),k(b,c),k(c,d),k(b,e),r&&r.m(b,null),g=!0},
						p(a,c){(!g||a.tip)&&t(d,c.tip),r&&r.p&&a.$$scope&&r.p(j(q,c,a),i(q,c,null)),
						(!g||a.clazz)&&f!==(f="___blur-tooltip "+c.clazz)&&(b.className=f)},i(a){g||(r&&r.i&&r.i(a),
							g=!0)},o(a){r&&r.o&&r.o(a),g=!1},d(a){a&&m(b),r&&r.d(a)}}}
			    function O(a,b,c){let{tip:d="",clazz:e=""}=b,{$$slots:g={},$$scope:f}=b;
			    return a.$set=a=>{"tip"in a&&c("tip",d=a.tip),"clazz"in a&&c("clazz",e=a.clazz),
					"$$scope"in a&&c("$$scope",f=a.$$scope)},{tip:d,clazz:e,$$slots:g,$$scope:f}}
			    function P(a){var b,c,d,e,f,g,h,i,j,o,q=new ia({props:{tip:"Switch mode",clazz:"___js-blur-ignore",
							$$slots:{default:[Q]},$$scope:{ctx:a}}}),r=new ia({props:{tip:"Blur selected text",
							clazz:"___js-blur-ignore",$$slots:{default:[R]},$$scope:{ctx:a}}}),
						s=new ia({props:{tip:"Clear all",clazz:"___js-blur-ignore",
								$$slots:{default:[S]},$$scope:{ctx:a}}}),
						t=a.showModal&&T(a),
						u=new ia({props:{tip:"About",clazz:"___js-blur-ignore",$$slots:{default:[V]},
								$$scope:{ctx:a}}}),v=new ia({props:{tip:"Close",clazz:"___js-blur-ignore",
								$$slots:{default:[W]},$$scope:{ctx:a}}});return{c(){b=n("div"),c=n("div"),
							q.$$.fragment.c(),d=p(),r.$$.fragment.c(),e=p(),s.$$.fragment.c(),f=p(),g=n("div"),h=p(),
						t&&t.c(),i=p(),u.$$.fragment.c(),j=p(),v.$$.fragment.c(),g.className="___js-blur-ignore ___blur-separator",
							c.className="___js-blur-ignore ___blur-control",b.className="___js-blur-ignore ___blur-container"},
						m(a,m){l(a,b,m),k(b,c),G(q,c,null),k(c,d),G(r,c,null),k(c,e),G(s,c,null),k(c,f),k(c,g),k(c,h),
						t&&t.m(c,null),k(c,i),G(u,c,null),k(c,j),G(v,c,null),o=!0},
						p(a,b){var d={};(a.$$scope||a.wandMode)&&(d.$$scope={changed:a,ctx:b}),q.$set(d);
						var e={};a.$$scope&&(e.$$scope={changed:a,ctx:b}),r.$set(e);var f={};
						a.$$scope&&(f.$$scope={changed:a,ctx:b}),s.$set(f),b.showModal?t?(t.p(a,b),
							t.i(1)):(t=T(b),t.c(),t.i(1),t.m(c,i)):t&&(D(),F(()=>{t.d(1),t=null}),
							t.o(1),E());var g={};a.$$scope&&(g.$$scope={changed:a,ctx:b}),u.$set(g);
							var h={};a.$$scope&&(h.$$scope={changed:a,ctx:b}),
								v.$set(h)},i(a){o||(q.$$.fragment.i(a),r.$$.fragment.i(a),
							s.$$.fragment.i(a),t&&t.i(),u.$$.fragment.i(a),v.$$.fragment.i(a),o=!0)},o(a){q.$$.fragment.o(a),
							r.$$.fragment.o(a),s.$$.fragment.o(a),t&&t.o(),u.$$.fragment.o(a),v.$$.fragment.o(a),o=!1},
						d(a){a&&m(b),q.$destroy(),r.$destroy(),s.$destroy(),t&&t.d(),u.$destroy(),v.$destroy()}}}
			    function Q(a){var b,c,d,e,f;return{c(){b=n("button"),c=n("img"),c.className="___js-blur-ignore",
							c.alt="Switch mode",
							c.src=d=a.wandMode?"data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNSIgaGVpZ2h0PSIyNSIgdmlld0JveD0iMCAwIDI1IDI1Ij48ZGVmcz48c3R5bGU+LmF7b3BhY2l0eTowLjc7fS5ie2ZpbGw6I2ZmZjt9LmN7ZmlsbDpub25lO308L3N0eWxlPjwvZGVmcz48ZyBjbGFzcz0iYSI+PHBhdGggY2xhc3M9ImIiIGQ9Ik0yMy4yLDUuOEgxOS42djQuOWEyLjM2MSwyLjM2MSwwLDAsMSwuMywzLjNsLS4zLjN2NC45aDMuNmEuNzg5Ljc4OSwwLDAsMCwuOC0uOGgwVjYuNUEuNzczLjc3MywwLDAsMCwyMy4yLDUuOFoiLz48cGF0aCBjbGFzcz0iYiIgZD0iTTE1LDE0LjNhMi4zNjEsMi4zNjEsMCwwLDEtLjMtMy4zbC4zLS4zVjUuOEgxLjhhLjc3My43NzMsMCwwLDAtLjguN1YxOC4zYS44Ni44NiwwLDAsMCwuOC44SDE1WiIvPjxwYXRoIGNsYXNzPSJiIiBkPSJNMTkuNSwyMi4xLDE4LDIwLjZWMTMuMmguMWEuOC44LDAsMCwwLDAtMS42SDE4VjQuM2wxLjUtMS41YS43ODUuNzg1LDAsMCwwLDAtMS4xaDBhLjc4NS43ODUsMCwwLDAtMS4xLDBMMTcuMywyLjgsMTYuMiwxLjdhLjc3OC43NzgsMCwwLDAtMS4xLDEuMWgwbDEuNSwxLjV2Ny40aC0uMWEuOC44LDAsMSwwLDAsMS42aC4xdjcuNEwxNSwyMi4xYS43ODEuNzgxLDAsMSwwLDEsMS4ybC4xLS4xLDEuMS0xLjEsMS4xLDEuMWEuNzgxLjc4MSwwLDAsMCwxLjItMUMxOS42LDIyLjIsMTkuNiwyMi4xLDE5LjUsMjIuMVoiLz48cmVjdCBjbGFzcz0iYyIgd2lkdGg9IjI1IiBoZWlnaHQ9IjI1Ii8+PC9nPjwvc3ZnPg==":"data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNSIgaGVpZ2h0PSIyNSIgdmlld0JveD0iMCAwIDI1IDI1Ij48ZGVmcz48c3R5bGU+LmF7b3BhY2l0eTowLjc7fS5ie2ZpbGw6I2ZmZjt9LmN7ZmlsbDpub25lO308L3N0eWxlPjwvZGVmcz48ZyBjbGFzcz0iYSI+PGNpcmNsZSBjbGFzcz0iYiIgY3g9IjAuNyIgY3k9IjAuNyIgcj0iMC43IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgyMi45IDguNCkiLz48cGF0aCBjbGFzcz0iYiIgZD0iTTIxLjMsNC40YS43NjQuNzY0LDAsMCwwLC41LS4yYy4xLS4yLjItLjMuMi0uNWEuNzY0Ljc2NCwwLDAsMC0uMi0uNS42NjguNjY4LDAsMCwwLTEsMCwuNzI1LjcyNSwwLDAsMCwwLDFBLjc2NC43NjQsMCwwLDAsMjEuMyw0LjRaIi8+PHBhdGggY2xhc3M9ImIiIGQ9Ik0yMS44LDE0YS43MDcuNzA3LDAsMCwwLTEsMSwuNzA3LjcwNywwLDAsMCwxLTFaIi8+PHBhdGggY2xhc3M9ImIiIGQ9Ik0xNS45LDIuMWEuNjg0LjY4NCwwLDAsMCwuNy0uNy43LjcsMCwxLDAtMS40LDBBLjY4NC42ODQsMCwwLDAsMTUuOSwyLjFaIi8+PHBhdGggY2xhc3M9ImIiIGQ9Ik0xNS45LDE3YS43LjcsMCwxLDAsLjcuN0EuNjg0LjY4NCwwLDAsMCwxNS45LDE3WiIvPjxwYXRoIGNsYXNzPSJiIiBkPSJNMTAuNSw0LjRhLjc2NC43NjQsMCwwLDAsLjUtLjIuNzI1LjcyNSwwLDAsMCwwLTEsLjY2OC42NjgsMCwwLDAtMSwwLC43NjQuNzY0LDAsMCwwLS4yLjVjMCwuMi4xLjMuMi41QS43NjQuNzY0LDAsMCwwLDEwLjUsNC40WiIvPjxjaXJjbGUgY2xhc3M9ImIiIGN4PSIwLjciIGN5PSIwLjciIHI9IjAuNyIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNi42IDguNCkiLz48cGF0aCBjbGFzcz0iYiIgZD0iTTE4LjEsNy41bC0uNi0uNmExLjY5MywxLjY5MywwLDAsMC0yLjMsMEwxMSwxMS4xYy0uMS4xLS4yLjEtLjIuMkwxLjIsMjAuOWExLjY5MywxLjY5MywwLDAsMCwwLDIuM2wuNi42YTEuNjA2LDEuNjA2LDAsMCwwLDEuMS41QTEuNjA2LDEuNjA2LDAsMCwwLDQsMjMuOEwxOCw5LjdBMS40ODgsMS40ODgsMCwwLDAsMTguMSw3LjVabS0uOSwxLjFjMCwuMSwwLC4xLS4xLjFoMGwtMy44LDMuOS0uOS0uOSwzLjgtMy45YS4xODcuMTg3LDAsMCwxLC4zLDBsLjYuNkEuMzEuMzEsMCwwLDEsMTcuMiw4LjZaIi8+PHBhdGggY2xhc3M9ImIiIGQ9Ik0xNS45LDUuOGEuNjg0LjY4NCwwLDAsMCwuNy0uN1YzLjJhLjY4NC42ODQsMCwwLDAtLjctLjdoMGEuNjg0LjY4NCwwLDAsMC0uNy43VjVBLjc3My43NzMsMCwwLDAsMTUuOSw1LjhaIi8+PHBhdGggY2xhc3M9ImIiIGQ9Ik0xNS45LDEzLjRoMGEuNjg0LjY4NCwwLDAsMC0uNy43djEuOGEuNy43LDAsMSwwLDEuNCwwVjE0LjFBLjY4NC42ODQsMCwwLDAsMTUuOSwxMy40WiIvPjxwYXRoIGNsYXNzPSJiIiBkPSJNMTEuMyw1LjVsMS4zLDEuM2EuNy43LDAsMCwwLDEuMi0uNS43NjQuNzY0LDAsMCwwLS4yLS41TDEyLjMsNC41YS42NjguNjY4LDAsMCwwLTEsMEEuNjY2LjY2NiwwLDAsMCwxMS4zLDUuNVoiLz48cGF0aCBjbGFzcz0iYiIgZD0iTTIwLjUsMTIuN2wtMS4zLTEuM2EuNjY4LjY2OCwwLDAsMC0xLDAsLjcyNS43MjUsMCwwLDAsMCwxbDEuMywxLjNhLjcyNS43MjUsMCwwLDAsMSwwQS42NjguNjY4LDAsMCwwLDIwLjUsMTIuN1oiLz48cGF0aCBjbGFzcz0iYiIgZD0iTTE4LjgsNi45YS43NjQuNzY0LDAsMCwwLC41LS4ybDEuMy0xLjNhLjY2OC42NjgsMCwwLDAsMC0xaDBhLjY2OC42NjgsMCwwLDAtMSwwTDE4LjMsNS43YS43MjUuNzI1LDAsMCwwLDAsMUMxOC40LDYuOSwxOC42LDYuOSwxOC44LDYuOVoiLz48cGF0aCBjbGFzcz0iYiIgZD0iTTkuMSw5LjhoMS44YS43LjcsMCwxLDAsMC0xLjRIOS4xYS43LjcsMCwwLDAsMCwxLjRaIi8+PHBhdGggY2xhc3M9ImIiIGQ9Ik0yMS44LDguNEgyMGEuNy43LDAsMSwwLDAsMS40aDEuOGEuNjg0LjY4NCwwLDAsMCwuNy0uN0EuNzUxLjc1MSwwLDAsMCwyMS44LDguNFoiLz48cmVjdCBjbGFzcz0iYyIgd2lkdGg9IjI1IiBoZWlnaHQ9IjI1Ii8+PC9nPjwvc3ZnPg==",
							b.className=e="___js-blur-ignore ___blur-button "+(a.wandMode?"___blur-selection":"___blur-wand")
							,f=r(b,"click",
							a.switchMode)},
						m(a,d){l(a,b,d),
							k(b,c)},
						p(a,f){a.wandMode&&d!==(d=f.wandMode?"data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNSIgaGVpZ2h0PSIyNSIgdmlld0JveD0iMCAwIDI1IDI1Ij48ZGVmcz48c3R5bGU+LmF7b3BhY2l0eTowLjc7fS5ie2ZpbGw6I2ZmZjt9LmN7ZmlsbDpub25lO308L3N0eWxlPjwvZGVmcz48ZyBjbGFzcz0iYSI+PHBhdGggY2xhc3M9ImIiIGQ9Ik0yMy4yLDUuOEgxOS42djQuOWEyLjM2MSwyLjM2MSwwLDAsMSwuMywzLjNsLS4zLjN2NC45aDMuNmEuNzg5Ljc4OSwwLDAsMCwuOC0uOGgwVjYuNUEuNzczLjc3MywwLDAsMCwyMy4yLDUuOFoiLz48cGF0aCBjbGFzcz0iYiIgZD0iTTE1LDE0LjNhMi4zNjEsMi4zNjEsMCwwLDEtLjMtMy4zbC4zLS4zVjUuOEgxLjhhLjc3My43NzMsMCwwLDAtLjguN1YxOC4zYS44Ni44NiwwLDAsMCwuOC44SDE1WiIvPjxwYXRoIGNsYXNzPSJiIiBkPSJNMTkuNSwyMi4xLDE4LDIwLjZWMTMuMmguMWEuOC44LDAsMCwwLDAtMS42SDE4VjQuM2wxLjUtMS41YS43ODUuNzg1LDAsMCwwLDAtMS4xaDBhLjc4NS43ODUsMCwwLDAtMS4xLDBMMTcuMywyLjgsMTYuMiwxLjdhLjc3OC43NzgsMCwwLDAtMS4xLDEuMWgwbDEuNSwxLjV2Ny40aC0uMWEuOC44LDAsMSwwLDAsMS42aC4xdjcuNEwxNSwyMi4xYS43ODEuNzgxLDAsMSwwLDEsMS4ybC4xLS4xLDEuMS0xLjEsMS4xLDEuMWEuNzgxLjc4MSwwLDAsMCwxLjItMUMxOS42LDIyLjIsMTkuNiwyMi4xLDE5LjUsMjIuMVoiLz48cmVjdCBjbGFzcz0iYyIgd2lkdGg9IjI1IiBoZWlnaHQ9IjI1Ii8+PC9nPjwvc3ZnPg==":"data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNSIgaGVpZ2h0PSIyNSIgdmlld0JveD0iMCAwIDI1IDI1Ij48ZGVmcz48c3R5bGU+LmF7b3BhY2l0eTowLjc7fS5ie2ZpbGw6I2ZmZjt9LmN7ZmlsbDpub25lO308L3N0eWxlPjwvZGVmcz48ZyBjbGFzcz0iYSI+PGNpcmNsZSBjbGFzcz0iYiIgY3g9IjAuNyIgY3k9IjAuNyIgcj0iMC43IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgyMi45IDguNCkiLz48cGF0aCBjbGFzcz0iYiIgZD0iTTIxLjMsNC40YS43NjQuNzY0LDAsMCwwLC41LS4yYy4xLS4yLjItLjMuMi0uNWEuNzY0Ljc2NCwwLDAsMC0uMi0uNS42NjguNjY4LDAsMCwwLTEsMCwuNzI1LjcyNSwwLDAsMCwwLDFBLjc2NC43NjQsMCwwLDAsMjEuMyw0LjRaIi8+PHBhdGggY2xhc3M9ImIiIGQ9Ik0yMS44LDE0YS43MDcuNzA3LDAsMCwwLTEsMSwuNzA3LjcwNywwLDAsMCwxLTFaIi8+PHBhdGggY2xhc3M9ImIiIGQ9Ik0xNS45LDIuMWEuNjg0LjY4NCwwLDAsMCwuNy0uNy43LjcsMCwxLDAtMS40LDBBLjY4NC42ODQsMCwwLDAsMTUuOSwyLjFaIi8+PHBhdGggY2xhc3M9ImIiIGQ9Ik0xNS45LDE3YS43LjcsMCwxLDAsLjcuN0EuNjg0LjY4NCwwLDAsMCwxNS45LDE3WiIvPjxwYXRoIGNsYXNzPSJiIiBkPSJNMTAuNSw0LjRhLjc2NC43NjQsMCwwLDAsLjUtLjIuNzI1LjcyNSwwLDAsMCwwLTEsLjY2OC42NjgsMCwwLDAtMSwwLC43NjQuNzY0LDAsMCwwLS4yLjVjMCwuMi4xLjMuMi41QS43NjQuNzY0LDAsMCwwLDEwLjUsNC40WiIvPjxjaXJjbGUgY2xhc3M9ImIiIGN4PSIwLjciIGN5PSIwLjciIHI9IjAuNyIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNi42IDguNCkiLz48cGF0aCBjbGFzcz0iYiIgZD0iTTE4LjEsNy41bC0uNi0uNmExLjY5MywxLjY5MywwLDAsMC0yLjMsMEwxMSwxMS4xYy0uMS4xLS4yLjEtLjIuMkwxLjIsMjAuOWExLjY5MywxLjY5MywwLDAsMCwwLDIuM2wuNi42YTEuNjA2LDEuNjA2LDAsMCwwLDEuMS41QTEuNjA2LDEuNjA2LDAsMCwwLDQsMjMuOEwxOCw5LjdBMS40ODgsMS40ODgsMCwwLDAsMTguMSw3LjVabS0uOSwxLjFjMCwuMSwwLC4xLS4xLjFoMGwtMy44LDMuOS0uOS0uOSwzLjgtMy45YS4xODcuMTg3LDAsMCwxLC4zLDBsLjYuNkEuMzEuMzEsMCwwLDEsMTcuMiw4LjZaIi8+PHBhdGggY2xhc3M9ImIiIGQ9Ik0xNS45LDUuOGEuNjg0LjY4NCwwLDAsMCwuNy0uN1YzLjJhLjY4NC42ODQsMCwwLDAtLjctLjdoMGEuNjg0LjY4NCwwLDAsMC0uNy43VjVBLjc3My43NzMsMCwwLDAsMTUuOSw1LjhaIi8+PHBhdGggY2xhc3M9ImIiIGQ9Ik0xNS45LDEzLjRoMGEuNjg0LjY4NCwwLDAsMC0uNy43djEuOGEuNy43LDAsMSwwLDEuNCwwVjE0LjFBLjY4NC42ODQsMCwwLDAsMTUuOSwxMy40WiIvPjxwYXRoIGNsYXNzPSJiIiBkPSJNMTEuMyw1LjVsMS4zLDEuM2EuNy43LDAsMCwwLDEuMi0uNS43NjQuNzY0LDAsMCwwLS4yLS41TDEyLjMsNC41YS42NjguNjY4LDAsMCwwLTEsMEEuNjY2LjY2NiwwLDAsMCwxMS4zLDUuNVoiLz48cGF0aCBjbGFzcz0iYiIgZD0iTTIwLjUsMTIuN2wtMS4zLTEuM2EuNjY4LjY2OCwwLDAsMC0xLDAsLjcyNS43MjUsMCwwLDAsMCwxbDEuMywxLjNhLjcyNS43MjUsMCwwLDAsMSwwQS42NjguNjY4LDAsMCwwLDIwLjUsMTIuN1oiLz48cGF0aCBjbGFzcz0iYiIgZD0iTTE4LjgsNi45YS43NjQuNzY0LDAsMCwwLC41LS4ybDEuMy0xLjNhLjY2OC42NjgsMCwwLDAsMC0xaDBhLjY2OC42NjgsMCwwLDAtMSwwTDE4LjMsNS43YS43MjUuNzI1LDAsMCwwLDAsMUMxOC40LDYuOSwxOC42LDYuOSwxOC44LDYuOVoiLz48cGF0aCBjbGFzcz0iYiIgZD0iTTkuMSw5LjhoMS44YS43LjcsMCwxLDAsMC0xLjRIOS4xYS43LjcsMCwwLDAsMCwxLjRaIi8+PHBhdGggY2xhc3M9ImIiIGQ9Ik0yMS44LDguNEgyMGEuNy43LDAsMSwwLDAsMS40aDEuOGEuNjg0LjY4NCwwLDAsMCwuNy0uN0EuNzUxLjc1MSwwLDAsMCwyMS44LDguNFoiLz48cmVjdCBjbGFzcz0iYyIgd2lkdGg9IjI1IiBoZWlnaHQ9IjI1Ii8+PC9nPjwvc3ZnPg==")&&(c.src=d),a.wandMode&&e!==(e="___js-blur-ignore ___blur-button "+(f.wandMode?"___blur-selection":"___blur-wand"))&&(b.className=e)},d(a){a&&m(b),f()}}}
			    function R(b){var c,d;return{c(){c=n("button"),c.innerHTML=`<img class="___js-blur-ignore" alt="Clear all" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNSIgaGVpZ2h0PSIyNSIgdmlld0JveD0iMCAwIDI1IDI1Ij48ZGVmcz48c3R5bGU+LmF7b3BhY2l0eTowLjc7fS5ie2ZpbGw6I2ZmZjt9LmN7ZmlsbDpub25lO308L3N0eWxlPjwvZGVmcz48ZyBjbGFzcz0iYSI+PHBhdGggY2xhc3M9ImIiIGQ9Ik0xNi41LDcuNGMtMS43LTIuMy0zLjMtNC4yLTMuNC00LjNsLS42LS42LS42LjZBNDkuOTA2LDQ5LjkwNiwwLDAsMCw4LjUsNy40QzYuMiwxMC42LDUsMTMuMSw1LDE0LjdWMTVhNy41LDcuNSwwLDAsMCwxNSwwdi0uMkMyMCwxMy4xLDE4LjgsMTAuNiwxNi41LDcuNFptLTMuNiwxMmgwYS44NzEuODcxLDAsMCwxLS44LS42Ljc2Ljc2LDAsMCwxLC42LS44LDMuMTk0LDMuMTk0LDAsMCwwLDIuOC0yLjkuNzA3LjcwNywwLDEsMSwxLjQuMkE0LjMxMiw0LjMxMiwwLDAsMSwxMi45LDE5LjRaIi8+PHJlY3QgY2xhc3M9ImMiIHdpZHRoPSIyNSIgaGVpZ2h0PSIyNSIvPjwvZz48L3N2Zz4=">`,
							c.className="___js-blur-ignore ___blur-button",d=r(c,"click",b.blurSelectedText)},m(a,b){l(a,c,b)},p:a,d(a){a&&m(c),d()}}}
			    function S(b){var c,d;return{c(){c=n("button"),c.innerHTML=`<img class="___js-blur-ignore" alt="Clear all" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNSIgaGVpZ2h0PSIyNSIgdmlld0JveD0iMCAwIDI1IDI1Ij48ZGVmcz48c3R5bGU+LmF7b3BhY2l0eTowLjc7fS5ie2ZpbGw6I2ZmZjt9LmN7ZmlsbDpub25lO308L3N0eWxlPjwvZGVmcz48ZyBjbGFzcz0iYSI+PHBhdGggY2xhc3M9ImIiIGQ9Ik0yMS4xLDIwLjVoLTdsNy42LTcuOWEyLjkxLDIuOTEsMCwwLDAsMC0zLjlMMTcsMy44YTIuNjg1LDIuNjg1LDAsMCwwLTMuOCwwTDMuMywxNGEyLjgwOCwyLjgwOCwwLDAsMCwwLDMuOWwyLjUsMi41SDMuMmEuNzEzLjcxMywwLDAsMC0uNy44Ljc3My43NzMsMCwwLDAsLjcuOEgyMS4xYS43MTMuNzEzLDAsMCwwLC43LS44QzIxLjksMjAuOCwyMS41LDIwLjUsMjEuMSwyMC41Wk00LjMsMTYuOWExLjI2OCwxLjI2OCwwLDAsMSwwLTEuOGw1LjctNiw2LjUsNi43TDEyLDIwLjVINy44WiIvPjxyZWN0IGNsYXNzPSJjIiB3aWR0aD0iMjUiIGhlaWdodD0iMjUiLz48L2c+PC9zdmc+">`,
							c.className="___js-blur-ignore ___blur-button",d=r(c,"click",b.clear)},m(a,b){l(a,c,b)},p:a,d(a){a&&m(c),d()}}}
			    function T(a){var b,c=new ha({props:{header:"About VTextBlur",$$slots:{default:[U]},$$scope:{ctx:a}}});return c.$on("close",a.closeAboutModal),{c(){c.$$.fragment.c()},m(a,d){G(c,a,d),b=!0},p(a,b){var d={};a.$$scope&&(d.$$scope={changed:a,ctx:b}),c.$set(d)},i(a){b||(c.$$.fragment.i(a),b=!0)},o(a){c.$$.fragment.o(a),b=!1},d(a){c.$destroy(a)}}}
			    function U(){var a,b=new ga({});return{c(){b.$$.fragment.c()},m(c,d){G(b,c,d),a=!0},i(c){a||(b.$$.fragment.i(c),a=!0)},o(c){b.$$.fragment.o(c),a=!1},d(a){b.$destroy(a)}}}function V(b){var c,d;return{c(){c=n("button"),
							c.innerHTML=`<img class="___js-blur-ignore" alt="Close" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNSIgaGVpZ2h0PSIyNSIgdmlld0JveD0iMCAwIDI1IDI1Ij48ZGVmcz48c3R5bGU+LmF7ZmlsbDojZmZmO30uYntmaWxsOm5vbmU7fTwvc3R5bGU+PC9kZWZzPjxwYXRoIGNsYXNzPSJhIiBkPSJNMTIuNSwyLjVhMTAsMTAsMCwxLDAsMTAsMTBBMTAuMDI5LDEwLjAyOSwwLDAsMCwxMi41LDIuNVpNMTMuNywxOUgxMS4zVjkuNWgyLjRTMTMuNywxOSwxMy43LDE5Wk0xMy41LDguMmExLjI4NCwxLjI4NCwwLDAsMS0xLC40aC0uMWExLjIyMiwxLjIyMiwwLDAsMS0uOS0uNCwxLjQ4OSwxLjQ4OSwwLDAsMS0uNC0uOSwxLjA4NiwxLjA4NiwwLDAsMSwuNC0uOSwxLjI4NCwxLjI4NCwwLDAsMSwxLS40LDEuNDMzLDEuNDMzLDAsMCwxLDEsLjQsMS44LDEuOCwwLDAsMSwuNC45QTEuMjY4LDEuMjY4LDAsMCwxLDEzLjUsOC4yWiIvPjxyZWN0IGNsYXNzPSJiIiB3aWR0aD0iMjUiIGhlaWdodD0iMjUiLz48L3N2Zz4=">`,
		c.className="___js-blur-ignore ___blur-button",d=r(c,"click",b.showAboutModal)},m(a,b){l(a,c,b)},p:a,d(a){a&&m(c),d()}}}
			    function W(b){var c,d;return{c(){c=n("button"),
							c.innerHTML=`<img class="___js-blur-ignore" alt="Close" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNSIgaGVpZ2h0PSIyNSIgdmlld0JveD0iMCAwIDI1IDI1Ij48ZGVmcz48c3R5bGU+LmF7b3BhY2l0eTowLjc7fS5ie2ZpbGw6I2ZmZjt9LmN7ZmlsbDpub25lO308L3N0eWxlPjwvZGVmcz48ZyBjbGFzcz0iYSI+PHBhdGggY2xhc3M9ImIiIGQ9Ik0xOC44LDIwLjMsMTIuNSwxNCw2LjIsMjAuM2EuOTcyLjk3MiwwLDAsMS0xLjQtLjEsMS4wNDksMS4wNDksMCwwLDEsMC0xLjNsNi4zLTYuM0w0LjgsNi4yQS45OS45OSwwLDAsMSw2LjIsNC44bDYuMyw2LjMsNi4zLTYuM2ExLjA3MSwxLjA3MSwwLDAsMSwxLjQtLjEsMS4wNjMsMS4wNjMsMCwwLDEsLjEsMS40bC0uMS4xLTYuMyw2LjMsNi4zLDYuM2EuOTcyLjk3MiwwLDAsMS0uMSwxLjQuOTQyLjk0MiwwLDAsMS0xLjMuMVoiLz48cmVjdCBjbGFzcz0iYyIgd2lkdGg9IjI1IiBoZWlnaHQ9IjI1Ii8+PC9nPjwvc3ZnPg==">`
							,c.className="___js-blur-ignore ___blur-button",d=r(c,"click",b.deactivate)},m(a,b){l(a,c,b)},p:a,d(a){a&&m(c),d()}}}
			    function X(a){var b,c,d=a.isActive&&P(a);return{c(){d&&d.c(),b=q()},m(a,e){d&&d.m(a,e),l(a,b,e),c=!0},p(a,c){c.isActive?d?(d.p(a,c),d.i(1)):(d=P(c),d.c(),d.i(1),d.m(b.parentNode,b)):d&&(D(),F(()=>{d.d(1),d=null}),d.o(1),E())},i(a){c||(d&&d.i(),c=!0)},o(a){d&&d.o(),c=!1},d(a){d&&d.d(a),a&&m(b)}}}
			    function Y(a,b,c){const d={cursorClass:"___blur-cursor",
						hoverClass:"___blur-hover",blurClass:"___blur-blur",
						ignoreClass:"___js-blur-ignore",markClass:"___blur-blur-mark"};
			    let f,g=!1,h=!0,i=!1;
			    const j=a=>{h&&(a.stopPropagation(),
						c("lastHoverEle",f=a.target),!f.classList.contains(d.ignoreClass)&&f.classList.add(d.hoverClass))},k=a=>{h&&(a.stopPropagation(),a.target.classList.remove(d.hoverClass))}, l=a=>{h&&(a.preventDefault(),a.stopPropagation(), !a.target.classList.contains(d.ignoreClass)&&a.target.classList.toggle(d.blurClass))}, m=a=>{a.preventDefault(),"Escape"===a.key&&o()},n=()=>{c("isActive",g=!0), p(),document.addEventListener("mouseover",j), document.addEventListener("mouseout",k), document.addEventListener("click",l), document.addEventListener("keydown",m)},
						o=()=>{c("isActive",g=!1),q(), document.removeEventListener("mouseover",j), document.removeEventListener("mouseout",k), document.removeEventListener("click",l), document.removeEventListener("keydown",m)},
						p=()=>{h&&document.documentElement.classList.add(d.cursorClass)},
						q=()=>{document.documentElement.classList.remove(d.cursorClass),
						f&&f.classList.remove(d.hoverClass)},
						r=()=>{g?o():n()},
						s=()=>{c("wandMode",
						h=!h),h?p():q()},
						t=()=>{ra(()=>{const a=document.createElement("mark");
						return a.classList.add(d.markClass),a})};
			    let u;
			    const v=()=>{c("currentWandMode",u=h),
						c("wandMode",h=!1),
							c("showModal",i=!0)},
						w=()=>{c("wandMode",h=u),
							c("showModal",i=!1)};
			    return x(()=>{o()}),
						{isActive:g,
							wandMode:h,
							showModal:i,
							activate:n,
							deactivate:o,
							toggle:r,
							blurSelection:()=>{c("wandMode",h=!1),
								c("isActive",g=!0),o(),n(),t()},
							switchMode:s,blurSelectedText:t,
							clear:()=>{document.querySelectorAll(`.${d.blurClass}`).forEach(a=>{a.classList.remove(d.blurClass)}),
								ma(document.querySelectorAll(`mark.${d.markClass}`))},
							showAboutModal:v,closeAboutModal:w}}let Z;
const $=[],_=Promise.resolve();let aa=!1;const ba=[],ca=[],da=[];let ea;class fa{$destroy(){H(this,!0),this.$destroy=a}$on(a,b){const c=this.$$.callbacks[a]||(this.$$.callbacks[a]=[]);return c.push(b),()=>{const a=c.indexOf(b);-1!==a&&c.splice(a,1)}}$set(){}}class ga extends fa{constructor(a){super(),J(this,a,null,K,g,[])}}class ha extends fa{constructor(a){super(),J(this,a,M,L,g,["header"])}}class ia extends fa{constructor(a){super(),J(this,a,O,N,g,["tip","clazz"])}}const ja=a=>{const b=a.parentNode;b&&b.removeChild(a)},ka=(a,b)=>{ja(a),b.parentNode.insertBefore(a,b),ja(b)},la=a=>{const b=document.createRange();b.selectNodeContents(a),ka(b.extractContents(),a)},ma=a=>{a.forEach(a=>{const b=a.parentNode;la(a),b.normalize()})},na=a=>{const b=a||document.body,c=b.ownerDocument||document,d=c.createTreeWalker(b,NodeFilter.SHOW_TEXT,null,!1),e=[];for(let b;b=d.nextNode();)e.push(b);return e},oa=a=>0<a.textContent.length,pa=a=>{const b=a.commonAncestorContainer;return na(b.parentNode||b).filter(b=>a.intersectsNode(b)&&oa(b))},qa=(a,b)=>{let c=b.startContainer,d=b.endContainer,e=b.startOffset,f=b.endOffset;return b=>{const g=document.createRange(),h=a.cloneNode();return g.selectNodeContents(b),b===c&&3===c.nodeType&&(g.setStart(b,e),c=h,e=0),b===d&&3===d.nodeType&&(g.setEnd(b,f),d=h,f=1),g.surroundContents(h),h}},ra=a=>{const b=document.getSelection();if(0==b.rangeCount)return;const c=b.getRangeAt(0);let d=pa(c),e=a();d.map(qa(e,c)),b.removeAllRanges()};class sa extends fa{constructor(a){super(),J(this,a,Y,X,g,["activate","toggle","blurSelection"])}get activate(){return this.$$.ctx.activate}get toggle(){return this.$$.ctx.toggle}get blurSelection(){return this.$$.ctx.blurSelection}}
			    var ta=(a=>{let b,c=!1;return function(){return c||(b=a.apply(this,arguments),c=!0),b}})(()=>{const a=document.createElement("div");return document.body.appendChild(a),new sa({target:a})});return ta});
