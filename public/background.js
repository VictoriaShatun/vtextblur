((ext) => {

       ext.browserAction.onClicked.addListener(tab => {
            ext.tabs.sendMessage(tab.id, {
                message: 'toggle'
        });
    });

    ext.contextMenus.create({
        title: 'Blur',
        // type: 'normal',
        contexts: ['selection'],
        onclick: (info, tab) => {
                ext.tabs.sendMessage(tab.id, {
                    message: 'blurSelection'
            });
        },
    });
})(chrome || browser);
