((ext) => {
    const msgHandler = (request, sender, sendResponse) => {
        switch (request.message) {
            case 'toggle':
                BlurPage().toggle();
                break;
            case 'blurSelection':
                BlurPage().blurSelection();
                break;
            default:
                break;
        }
    };

    ext.runtime.onMessage.addListener(msgHandler);
})(chrome || browser);
